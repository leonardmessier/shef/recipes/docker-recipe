include .env
export

CONTAINER_TEST := $(shell command -v container-structure-test 2>/dev/null)

build-docker:
	@docker-compose build docker-recipe

test-local:
	@shef install-recipe captainquirk/docker-recipe --path $(shell pwd)

test:
ifndef CONTAINER_TEST
	@echo "container-structure-test is not available. Follow instructions here : https://github.com/GoogleContainerTools/container-structure-test"
	exit 1
endif

	@container-structure-test test --image registry.gitlab.com/leonardmessier/shef/recipes/docker-recipe:$(RECIPE_VERSION) --config tests/acceptance/cst.yml
	@echo "\033[32m✓\033[0m Acceptance tests passed"

.PHONY: build install
